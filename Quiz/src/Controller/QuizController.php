<?php
/**
 * Created by PhpStorm.
 * User: Dom
 * Date: 01.07.2018
 * Time: 22:34
 */

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Answers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;


class QuizController  extends Controller
{
    /**
     * @Route("/quiz/start", name="quiz_start")
     */
    public function indexAction(Request $request)
    {
        $number=$request->get('number');
        $level=$request->get('level');
        $repository = $this->getDoctrine()
            ->getRepository(Question::class);
        $query = $repository->createQueryBuilder('q')

            ->Where('q.questionLevel = :level')
            ->andWhere('q.active = :active')
            ->setParameter('active',1)
            ->setParameter('level', $level)
            ->getQuery();
        $questions= $query->getResult();

        $em = $this->getDoctrine()->getManager();
        $answers = $em->getRepository(Answers::class)->findAll();
        if(count($questions)<$number){
        return $this->render("errorQuizGame.html.twig");
        }
        else{
            $shuffledQuestions=shuffle($questions);
            $choosedQuestions=array_slice($questions,0,$number);
        }

        return $this->render("quizGame.html.twig", array('answers' => $answers, 'questions' => $choosedQuestions));

    }

}