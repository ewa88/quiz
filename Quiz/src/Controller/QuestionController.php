<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Answers;
use App\Form\QuestionType;
use App\Form\QuestionEditionType;
use function PHPSTORM_META\type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Response;


class QuestionController extends Controller
{
    /**
     * @Route("/question/add", name="question_add")
     */
    public function IndexAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $inputId=$request->get('id');
        $question = $em->getRepository(Question::class)->find($inputId);

        if (!$question)  {
            $question = new Question();
            $question->setCreated(new \DateTime());
            $question->SetId($inputId);
            $form = $this->createForm(QuestionType::class, $question);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($question);
                $em->flush();
                return $this->redirectToRoute('question_list');
            }
        }
        else{
            return $this->render('errorPage.html.twig');
        }

        return $this->render('addQuestion.html.twig', array(
            'form' => $form->createView()));
    }
    /**
     * @Route("/question/list", name="question_list")
     */
    public function listOfQuestionAction(){

        $em = $this->getDoctrine()->getManager();
        $questions = $em->getRepository(Question::class)->findAll();
       return $this->render("questionList.html.twig", array('questions' => $questions));
    }

    /**
     * @Route("/question/edit/{id}", name="question_edit")
     */
    public function editAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $question = $entityManager->getRepository(Question::class)->find($id);
        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);
        $question->setCreated(new \DateTime());
        //add unique question id
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($question);
            $entityManager->flush();
            return $this->redirectToRoute('question_list');
        }

        return $this->render('addQuestion.html.twig', array(
            'form' => $form->createView()));
    }


    /**
     * @Route("/question/delete/{id}", name="question_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository(Question::class)->find($id);
        $em->remove($question);
        $em->flush();

        return $this->redirectToRoute('question_list');
    }};