<?php

namespace App\Controller;

use App\Entity\Answers;
use App\Entity\Question;
use App\Form\AnswerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use function PHPSTORM_META\type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\AbstractType;


class AnswerController extends Controller
{
    /**
     * @Route("/answer/add/{id}", name="answer_add")
     */
    public function indexAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $answer = new Answers();
        $question = $em->getRepository(Question::class)->find($id);
        $numberOfAnswers = count($question->getAnswerContent());
        $answer->SetAnswerOrder($numberOfAnswers + 1);
        $answer->setQuestion($question);
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($answer);
            $em->persist($question);
            $em->flush();
            return $this->redirectToRoute('question_list');
        }
        return $this->render('addAnswer.html.twig', array(
            'form' => $form->createView()));
    }

    /**
     * @Route("/answer/list", name="answer_list")
     */
    public function listOfAnswersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $answers = $em->getRepository(Answers::class)->findAll();
        $questions = $em->getRepository(Question::class)->findAll();
        return $this->render("answerList.html.twig", array('answers' => $answers, 'questions' => $questions));
    }

    /**
     * @Route("/answer/delete/{id}", name="answer_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $answer = $em->getRepository(Answers::class)->find($id);
        $em->remove($answer);
        $em->flush();

        return $this->redirectToRoute('answer_list');
    }

    /**
     * @Route("/answer/edit/{id}", name="answer_edit")
     */
    public function editAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $answer = $entityManager->getRepository(Answers::class)->find($id);
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($answer);
            $entityManager->flush();
            return $this->redirectToRoute('answer_list');
        }

        return $this->render('addAnswer.html.twig', array(
            'form' => $form->createView()));

    }



    }

