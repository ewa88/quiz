<?php
/**
 * Created by PhpStorm.
 * User: Dom
 * Date: 01.07.2018
 * Time: 15:26
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;

class DefaultController  extends Controller
{
    /**
     * @Route("/admin")
     */
    public function admin()
    {
        return $this->render('homePage.html.twig');
    }

    /**
     * @Route("/user")
     */
    public function user()
    {
        return $this->render('startQuiz.html.twig');
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

}