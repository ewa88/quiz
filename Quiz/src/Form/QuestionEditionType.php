<?php

namespace App\Form;

use function PHPSTORM_META\type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BooleanType;

class QuestionEditionType extends AbstractType
{
    public function getName() {
        return 'questionEdition_form';
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', TextType::class)
            ->add('content', TextType::class)
            ->add('active', CheckboxType::class, array(
                'label' => 'Pytanie aktywne',
                'required' => false,))
            ->add('questionLevel', ChoiceType::class, array(
                'choices'  => array(
                    'Początkujacy' => 1,
                    'Zaawansowany' => 2,
                    'Expert' => 3,
                )))
            ->add('isSingleOrMulti', CheckboxType::class, array(
                'label' => 'Pytanie wielokrotnego wyboru',
                'required' => false,));


    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Question'
        ));
    }
}
