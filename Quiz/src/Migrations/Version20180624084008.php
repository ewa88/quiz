<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180624084008 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users_quiz (id INT AUTO_INCREMENT NOT NULL, answers_id INT NOT NULL, users_id INT NOT NULL, created DATETIME NOT NULL, INDEX IDX_6B9BB49C79BF1BCE (answers_id), INDEX IDX_6B9BB49C67B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_quiz ADD CONSTRAINT FK_6B9BB49C79BF1BCE FOREIGN KEY (answers_id) REFERENCES answers (id)');
        $this->addSql('ALTER TABLE users_quiz ADD CONSTRAINT FK_6B9BB49C67B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE users_quiz');
    }
}
