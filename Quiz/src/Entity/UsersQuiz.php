<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersQuizRepository")
 */
class UsersQuiz
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Answers", inversedBy="getUserWithAnswer")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Answers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="getQuizByUser")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Users;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function getId()
    {
        return $this->id;
    }

    public function getAnswers(): ?Answers
    {
        return $this->Answers;
    }

    public function setAnswers(?Answers $Answers): self
    {
        $this->Answers = $Answers;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->Users;
    }

    public function setUsers(?Users $Users): self
    {
        $this->Users = $Users;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }
}
