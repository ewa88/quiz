<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnswersRepository")
 */
class Answers
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="answerContent")
     * @ORM\JoinColumn(nullable=false)
     */

    private $Question;

    /**
     * @ORM\Column(type="boolean")
     */
    private $correct;

    /**
     * @ORM\Column(type="integer")
     */
    private $answerOrder;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UsersQuiz", mappedBy="Answers", orphanRemoval=true)
     */
    private $getUserWithAnswer;

    public function __construct()
    {
        $this->getUserWithAnswer = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getQuestion(): ?Question
    {
        return $this->Question;
    }

    public function setQuestion(?Question $Question): self
    {
        $this->Question = $Question;

        return $this;
    }

    public function getCorrect(): ?bool
    {
        return $this->correct;
    }

    public function setCorrect(bool $correct): self
    {
        $this->correct = $correct;

        return $this;
    }

    public function getAnswerOrder(): ?int
    {
        return $this->answerOrder;
    }

    public function setAnswerOrder(int $answerOrder): self
    {
        $this->answerOrder = $answerOrder;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|UsersQuiz[]
     */
    public function getGetUserWithAnswer(): Collection
    {
        return $this->getUserWithAnswer;
    }

    public function addGetUserWithAnswer(UsersQuiz $getUserWithAnswer): self
    {
        if (!$this->getUserWithAnswer->contains($getUserWithAnswer)) {
            $this->getUserWithAnswer[] = $getUserWithAnswer;
            $getUserWithAnswer->setAnswers($this);
        }

        return $this;
    }

    public function removeGetUserWithAnswer(UsersQuiz $getUserWithAnswer): self
    {
        if ($this->getUserWithAnswer->contains($getUserWithAnswer)) {
            $this->getUserWithAnswer->removeElement($getUserWithAnswer);
            // set the owning side to null (unless already changed)
            if ($getUserWithAnswer->getAnswers() === $this) {
                $getUserWithAnswer->setAnswers(null);
            }
        }

        return $this;
    }
}
