<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $nickname;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="boolean")
     */
    private $quizFinish;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quizResult;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserDetails", mappedBy="Users", cascade={"persist", "remove"})
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UsersQuiz", mappedBy="Users", orphanRemoval=true)
     */
    private $getQuizByUser;

    public function __construct()
    {
        $this->getQuizByUser = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getQuizFinish(): ?bool
    {
        return $this->quizFinish;
    }

    public function setQuizFinish(bool $quizFinish): self
    {
        $this->quizFinish = $quizFinish;

        return $this;
    }

    public function getQuizResult(): ?int
    {
        return $this->quizResult;
    }

    public function setQuizResult(?int $quizResult): self
    {
        $this->quizResult = $quizResult;

        return $this;
    }

    public function getRole(): ?UserDetails
    {
        return $this->role;
    }

    public function setRole(UserDetails $role): self
    {
        $this->role = $role;

        // set the owning side of the relation if necessary
        if ($this !== $role->getUsers()) {
            $role->setUsers($this);
        }

        return $this;
    }

    /**
     * @return Collection|UsersQuiz[]
     */
    public function getGetQuizByUser(): Collection
    {
        return $this->getQuizByUser;
    }

    public function addGetQuizByUser(UsersQuiz $getQuizByUser): self
    {
        if (!$this->getQuizByUser->contains($getQuizByUser)) {
            $this->getQuizByUser[] = $getQuizByUser;
            $getQuizByUser->setUsers($this);
        }

        return $this;
    }

    public function removeGetQuizByUser(UsersQuiz $getQuizByUser): self
    {
        if ($this->getQuizByUser->contains($getQuizByUser)) {
            $this->getQuizByUser->removeElement($getQuizByUser);
            // set the owning side to null (unless already changed)
            if ($getQuizByUser->getUsers() === $this) {
                $getQuizByUser->setUsers(null);
            }
        }

        return $this;
    }
}
