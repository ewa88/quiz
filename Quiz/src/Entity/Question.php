<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="smallint")
     */
    private $questionLevel;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSingleOrMulti;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Answers", mappedBy="Question", orphanRemoval=true)
     */
    private $answerContent;

    public function __construct()
    {
        $this->answerContent = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getQuestionLevel(): ?int
    {
        return $this->questionLevel;
    }

    public function setQuestionLevel(int $questionLevel): self
    {
        $this->questionLevel = $questionLevel;

        return $this;
    }

    public function getIsSingleOrMulti(): ?bool
    {
        return $this->isSingleOrMulti;
    }

    public function setIsSingleOrMulti(bool $isSingleOrMulti): self
    {
        $this->isSingleOrMulti = $isSingleOrMulti;

        return $this;
    }

    /**
     * @return Collection|Answers[]
     */
    public function getAnswerContent(): Collection
    {
        return $this->answerContent;
    }

    public function addAnswerContent(Answers $answerContent): self
    {
        if (!$this->answerContent->contains($answerContent)) {
            $this->answerContent[] = $answerContent;
            $answerContent->setQuestion($this);
        }

        return $this;
    }

    public function removeAnswerContent(Answers $answerContent): self
    {
        if ($this->answerContent->contains($answerContent)) {
            $this->answerContent->removeElement($answerContent);
            // set the owning side to null (unless already changed)
            if ($answerContent->getQuestion() === $this) {
                $answerContent->setQuestion(null);
            }
        }

        return $this;
    }
}
