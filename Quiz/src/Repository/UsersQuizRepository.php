<?php

namespace App\Repository;

use App\Entity\UsersQuiz;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UsersQuiz|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersQuiz|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersQuiz[]    findAll()
 * @method UsersQuiz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersQuizRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UsersQuiz::class);
    }

//    /**
//     * @return UsersQuiz[] Returns an array of UsersQuiz objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersQuiz
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
